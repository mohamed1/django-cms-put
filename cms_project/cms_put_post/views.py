from django.http import HttpResponse, Http404
from .models import Contenido

# Create your views here.
from django.views.decorators.csrf import csrf_exempt  # Saltar mecanismo de seguridad CSRF

# Create your views here.
def index(request):
    return HttpResponse("<h1>Este es mi primer CMS</h1>")

formulario_html ='<form action="" method="POST">'\
                  '<div>'\
                    '</label><h1>Introduce el valor del recurso<h1></label>'\
                    '<input name="valor" autocomplete="name"/>'\
                  '</div>'\
                    '<button>Enviar</button>'\
                  '</div>'\
                '</form>'

@csrf_exempt  #salto mecanismo de seguridad
def get_resource(request, recurso):
    try:
        contenido = Contenido.objects.get(clave=recurso) #¿existe ya una clave con ese recurso?
        # POST o PUT
        if request.method == "POST":
            valor = request.POST.get("valor", '')
            if valor == '':
                return Http404('El valor introducido no es válido')
            contenido.valor = valor
            contenido.save()
            response = "El recurso pedido es: " + contenido.clave + ". Su valor es: " + valor \
                       + ".Su identificador es: " + str(contenido.id)

            return HttpResponse(response + formulario_html)

        else:
            response = "El recurso pedido es: " + contenido.clave + ". Su valor es: " + contenido.valor \
                           + ".Su identificador es: " + str(contenido.id)

            return HttpResponse(response + formulario_html)

    except Contenido.DoesNotExist: #no existe el recurso
        contenido = Contenido(clave=recurso)
        contenido.save()
        response = '<h1>El recurso introducido no existía, pero ya lo hemos creado<h1>'
        return HttpResponse(response + formulario_html)



